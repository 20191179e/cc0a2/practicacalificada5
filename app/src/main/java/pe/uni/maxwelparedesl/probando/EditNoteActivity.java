package pe.uni.maxwelparedesl.probando;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

public class EditNoteActivity extends AppCompatActivity {

    EditText editNoteTitle,editNoteSubTitle;
    FloatingActionButton floatingActionButtonUpdate, floatingActionButtonDelete;
    String titulo, descripcion;
    String id;
    LinearLayout linearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);

        editNoteTitle = findViewById(R.id.edit_note_title);
        editNoteSubTitle = findViewById(R.id.edit_note_subtitle);
        floatingActionButtonUpdate = findViewById(R.id.floating_action_button_update);
        floatingActionButtonDelete = findViewById(R.id.floating_action_button_delete);
        linearLayout = findViewById(R.id.linear_layout_edit_note);

        Intent in = getIntent();

        id = in.getStringExtra("identification");

        AdminSQLiteOpenHelper adminSQLiteOpenHelper = new AdminSQLiteOpenHelper(this,"notas",null,1);
        SQLiteDatabase sqLiteDatabase = adminSQLiteOpenHelper.getReadableDatabase();

        @SuppressLint("Recycle") Cursor cursor = sqLiteDatabase.rawQuery("SELECT title,subtitle FROM notita where id="+id,null);

        if(cursor.moveToFirst()) {
            editNoteTitle.setText(cursor.getString(0));
            editNoteSubTitle.setText(cursor.getString(1));
            sqLiteDatabase.close();
        }else {
            Toast.makeText(getApplicationContext(), id, Toast.LENGTH_SHORT).show();
        }

       floatingActionButtonUpdate.setOnClickListener(v -> modificar() );
        floatingActionButtonDelete.setOnClickListener(v-> eliminar() );
        
    }

    private void modificar(){

        AdminSQLiteOpenHelper adminSQLiteOpenHelper = new AdminSQLiteOpenHelper(this,"notas",null,1);
        SQLiteDatabase sqLiteDatabase = adminSQLiteOpenHelper.getReadableDatabase();

        titulo = editNoteTitle.getText().toString();
        descripcion = editNoteSubTitle.getText().toString();

        if(!titulo.isEmpty() && !descripcion.isEmpty()){

            ContentValues registry = new ContentValues();
            registry.put("title",titulo);
            registry.put("subtitle",descripcion);

            int result = sqLiteDatabase.update("notita",registry,"id="+id,null);
            sqLiteDatabase.close();
            if(result==1) {
                Snackbar.make(linearLayout,R.string.nota_actualizada,Snackbar.LENGTH_LONG).show();
            }
            else Toast.makeText(getApplicationContext(), R.string.nota_no_actualizada, Toast.LENGTH_SHORT).show();
        }
    }
    private void eliminar(){
        AdminSQLiteOpenHelper adminSQLiteOpenHelper = new AdminSQLiteOpenHelper(this,"notas",null,1);
        SQLiteDatabase sqLiteDatabase = adminSQLiteOpenHelper.getReadableDatabase();

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle(R.string.pregunta_si_elimina).setNegativeButton(R.string.cancelar,(dialog,which)-> dialog.cancel()).setPositiveButton(R.string.aceptar,(dialog, which) -> {
            int result = sqLiteDatabase.delete("notita","id="+id,null);
            if(result==1) {

                Snackbar.make(linearLayout,R.string.nota_eliminada,Snackbar.LENGTH_LONG).show();
                Intent inden = new Intent(EditNoteActivity.this,NotesActivity.class);
                inden.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(inden);

            }

            else Toast.makeText(getApplicationContext(), R.string.nota_no_eliminada, Toast.LENGTH_SHORT).show();

        }).show().create();

    }
}