package pe.uni.maxwelparedesl.probando;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;


public class OneNoteActivity extends AppCompatActivity {

    FloatingActionButton floatingActionButtonCheck;
    EditText editTextTitle, editTextSubTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_note);


        floatingActionButtonCheck = findViewById(R.id.floating_action_button_check);
        editTextTitle = findViewById(R.id.one_note_title);
        editTextSubTitle = findViewById(R.id.one_note_subtitle);


         guardaNota();

    }
    private void guardaNota(){
        floatingActionButtonCheck.setOnClickListener(v->{
            AdminSQLiteOpenHelper adminSQLiteOpenHelper = new AdminSQLiteOpenHelper(this,"notas",null,1);
            SQLiteDatabase sqLiteDatabase = adminSQLiteOpenHelper.getWritableDatabase();

            String titleNote = editTextTitle.getText().toString();
            String subTitleNote = editTextSubTitle.getText().toString();

            if(!titleNote.isEmpty() && !subTitleNote.isEmpty()){
                ContentValues registry = new ContentValues();
                registry.put("title",titleNote);
                registry.put("subtitle",subTitleNote);
                sqLiteDatabase.insert("notita",null,registry);
                sqLiteDatabase.close();
                editTextTitle.setText("");
                editTextSubTitle.setText("");
                Toast.makeText(this,R.string.guardar ,Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this,R.string.llenar,Toast.LENGTH_SHORT).show();
            }

        });

    }
}