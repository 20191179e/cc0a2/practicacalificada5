package pe.uni.maxwelparedesl.probando;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class GridAdapter extends BaseAdapter implements Filterable {

    Context context;
    ArrayList<String> title;
    ArrayList<String> titleFiltered;
    ArrayList<String> subTitle;

    public GridAdapter(Context context, ArrayList<String> title, ArrayList<String> subTitle) {
        this.context = context;
        this.title = title;
        this.titleFiltered = title;
        this.subTitle = subTitle;
    }

    @Override
    public int getCount() {
        return titleFiltered.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_view_layout, parent, false);
        TextView titleView = view.findViewById(R.id.title_card);
        TextView subTitleView = view.findViewById(R.id.subTitle_card);

        titleView.setText(titleFiltered.get(position));
        subTitleView.setText(subTitle.get(position));

        return view;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();

                if(constraint==null||constraint.length()==0){
                    filterResults.count = title.size();
                    filterResults.values = title;
                } else {
                    String searchStr = constraint.toString().toLowerCase();
                    List<String> result = new ArrayList<>();

                    for (String a : title) {
                        if(a.contains(searchStr)){
                                result.add(a);
                        }
                        filterResults.count = result.size();
                        filterResults.values = result;
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                titleFiltered = (ArrayList<String>) results.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }
}
