package pe.uni.maxwelparedesl.probando;

import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.SearchView;
import android.widget.Toast;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;

public class NotesActivity extends AppCompatActivity {
    FloatingActionButton floatingActionButton;
    SearchView searchView;
    GridView gridView;
    ArrayList<String> title = new ArrayList<>();
    ArrayList<String> subTitle = new ArrayList<>();
    ArrayList<String> itemIds = new ArrayList<>();

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);

        gridView = findViewById(R.id.grid_view);
        floatingActionButton = findViewById(R.id.floating_action_button);
        searchView = findViewById(R.id.search_view);

        rellenarTarjeta();

        GridAdapter gridAdapter = new GridAdapter(this, title, subTitle);

        gridView.setAdapter(gridAdapter);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                gridAdapter.getFilter().filter(newText);

                return false;
            }
        });

       gridView.setOnItemLongClickListener((parent, view, position, id) -> {
           Intent i = new Intent(NotesActivity.this,EditNoteActivity.class);
           ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, gridView, "transition_grid");
           i.putExtra("identification",String.valueOf(itemIds.get(position)));
           startActivity(i, options.toBundle());
           return false;
        });

        floatingActionButton.setOnClickListener(v -> {

            Intent intent = new Intent(NotesActivity.this, OneNoteActivity.class);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, floatingActionButton, "transition_fab");
            startActivity(intent, options.toBundle());
        });
    }


    protected void rellenarTarjeta() {

        AdminSQLiteOpenHelper adminSQLiteOpenHelper = new AdminSQLiteOpenHelper(this,"notas",null,1);
        SQLiteDatabase sqLiteDatabase = adminSQLiteOpenHelper.getReadableDatabase();

        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM notita",null);

        if(cursor.moveToLast()){

            do{
                itemIds.add(String.valueOf(cursor.getString(0)));
                title.add(cursor.getString(1));
                subTitle.add(cursor.getString(2));
            }while(cursor.moveToPrevious());

        }

        cursor.close();

    }


}