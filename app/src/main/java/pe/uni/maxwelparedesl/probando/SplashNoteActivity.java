package pe.uni.maxwelparedesl.probando;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

@SuppressLint("CustomSplashScreen")
public class SplashNoteActivity extends AppCompatActivity {

    ImageView imageView;
    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_note);

        imageView = findViewById(R.id.image_splash);

        animation = AnimationUtils.loadAnimation(this,R.anim.noteanimation);

        imageView.setAnimation(animation);

        new CountDownTimer(4000,1000){

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent inte = new Intent(SplashNoteActivity.this,MainActivity.class);
                startActivity(inte);
                finish();
            }
        }.start();

    }
}